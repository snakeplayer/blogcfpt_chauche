<?php
const DB_HOST = "127.0.0.1:3306";
const DB_NAME = "db_blog_cfpt";
const DB_USER = "root";
const DB_PASSWORD = "";
const IMG_PATH = './img/';

function getConnexion()
{
    static $dbh = null;
    if ($dbh === null)
    {
        try {
            $dbh = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASSWORD);
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            print "Erreur !: " . $e->getMessage() . "<br/>";
            die();
        }
    }
    return $dbh;
}

// ----------------------------------------------------- INSERT DATA -----------------------------------------------------------
function insertPost($text)
{
    $dbh = getConnexion();

    $req = $dbh->prepare("INSERT INTO `post` (`textPost`) VALUES (:text);");
    $req->execute(array(
        'text' => $text
    ));

    return $dbh->lastInsertId();
}

function insertImage($imagePath, $idPost)
{
    $dbh = getConnexion();

    $req = $dbh->prepare("INSERT INTO `image` (`linkImage`, `idPost`) VALUES (:linkImage, :idPost);");
    $req->execute(array(
        'linkImage' => $imagePath,
        'idPost' => $idPost
    ));
}

function insertTextAndImages($text, $tabImages)
{
    $dbh = getConnexion();

    try
    {
        $dbh->beginTransaction();

        // Insert post
        $idPost = insertPost($text);

        foreach ($tabImages as $value)
        {
            insertImage(IMG_PATH.$value, $idPost);
        }

        $dbh->commit();

    } catch (Exception $e)
    {
        $dbh->rollBack();
    }
}


// ----------------------------------------------------- GET DATA -----------------------------------------------------------
function getPosts()
{
    $dbh = getConnexion();
    $sql =  "SELECT * FROM post ORDER BY datePost DESC";
    return $dbh->query($sql, PDO::FETCH_ASSOC);
}

function getImagesByPostId($idPost)
{
    $dbh = getConnexion();

    $req = $dbh->prepare("SELECT * FROM image WHERE idPost = :idPost");
    $req->execute(array(
        'idPost' => $idPost
    ));

    return $req->fetchAll(PDO::FETCH_ASSOC);
}

// ----------------------------------------------------- UPDATE POST -----------------------------------------------------------
function editPost($idPost, $textPost)
{
    $dbh = getConnexion();
    $req = $dbh->prepare("UPDATE post SET textPost=:textPost WHERE idPost=:idPost;");
    $req->execute(array(
        'textPost' => $textPost,
        'idPost' => $idPost
    ));
}

function addImagesToPost($idPost, $tabImages)
{
    $dbh = getConnexion();

    try
    {
        $dbh->beginTransaction();

        foreach ($tabImages as $value)
        {
            insertImage(IMG_PATH.$value, $idPost);
        }

        $dbh->commit();

    } catch (Exception $e)
    {
        $dbh->rollBack();
    }
}

// ----------------------------------------------------- DELETE IMAGES -----------------------------------------------------------
function deleteImageDB($idImage)
{
    $dbh = getConnexion();

    $req = $dbh->prepare("DELETE FROM image WHERE  idImage = :idImage;");
    $req->execute(array(
        'idImage' => $idImage
    ));
}

function deleteImageFile($fileName)
{
    @unlink($fileName);
}

function deleteImagesFromPostId($idPost)
{
    $dbh = getConnexion();

    //Delete image files
    $tabImages = getImagesByPostId($idPost);

    foreach ($tabImages as $value)
    {
        @unlink($value['linkImage']);
    }

    // Delete images from DB
    $req = $dbh->prepare("DELETE FROM image WHERE  idPost = :idPost;");
    $req->execute(array(
        'idPost' => $idPost
    ));
}

// ----------------------------------------------------- DELETE POST -----------------------------------------------------------
function deletePost($idPost)
{
    $dbh = getConnexion();

    deleteImagesFromPostId($idPost);

    $req = $dbh->prepare("DELETE FROM post WHERE  idPost = :idPost;");
    $req->execute(array(
        'idPost' => $idPost
    ));
}

?>
