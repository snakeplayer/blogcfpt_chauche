<?php

function manageImages($tabImages, $maxWidth, $maxHeight, $destPath)
{
    $tabSavedImages;

    // Going through the images array
    foreach ($tabImages['image_post']['error'] as $key => $error)
    {
        // If there is no error
        if ($error == UPLOAD_ERR_OK)
        {
            // If image type is correct
            $m = mime_content_type($tabImages['image_post']['tmp_name'][$key]);
            if (/*$m == 'image/png' || */$m == 'image/jpe' || $m == 'image/jpeg' || $m == 'image/jpg' /* || $m == 'image/bmp'*/)
            {
                // Computing new sizes
                $width = $maxWidth;
                $height = $maxHeight;

                list($widthOrig, $heightOrig) = getimagesize($tabImages['image_post']['tmp_name'][$key]);

                $ratioOrig = $widthOrig/$heightOrig;

                if ($width/$height > $ratioOrig) {
                   $width = $height*$ratioOrig;
                } else {
                   $height = $width/$ratioOrig;
                }

                // Creation of a new image with good proportions
                $imageDst = imagecreatetruecolor($width, $height);
                $imageSrc = imagecreatefromjpeg($tabImages['image_post']['tmp_name'][$key]);
                imagecopyresampled($imageDst, $imageSrc, 0, 0, 0, 0, $width, $height, $widthOrig, $heightOrig);

                // Save the image name to the tab
                $uniqFileName = uniqid();
                $tabSavedImages[] = $uniqFileName;

                // Create the new image in at the path
                imagejpeg($imageDst, $destPath.$uniqFileName, 100);
            }
            else
            {
                throw new Exception("Une erreur est survenue lors du déplacement du fichier temporaire.");
            }
        }
        else
        {
            throw new Exception("Le fichier n'est pas du bon type.");
        }
    }
    return $tabSavedImages;
}

?>
