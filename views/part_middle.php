<div class="uk-card uk-card-default uk-width-2-3 uk-margin-small-top uk-padding-small uk-margin-large-bottom" style="background-color:#f0f0f0">
    <div uk-grid>

        <!-- MENU DE GAUCHE -->
        <?php include_once 'part_middle_left.php'; ?>

        <!-- DROITE DEBUT -->
        <div class="uk-width-4-5">

            <!-- BANNIERE -->
            <?php include_once 'part_middle_banner.php'; ?>

            <div uk-grid>

                <!-- STATUT -->
                <?php include_once 'part_middle_new_statut.php'; ?>

                <!-- A PROPOS -->
                <?php include_once 'part_middle_about.php'; ?>

                <!-- AFFICHAGE DES STATUTS -->
                <?php include_once 'part_middle_show_statuts.php'; ?>

            </div>

        </div>
        <!-- DROITE FIN -->

    </div>
</div>
