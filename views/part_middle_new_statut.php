<div class="uk-card uk-card-default uk-width-2-3 uk-margin-small-top uk-padding-remove-left uk-margin-medium-left uk-background-muted">
    <div class="uk-card uk-card-default uk-padding-small uk-width-1-1" style="background-color:#d9d9d9">
        Statut
        <span uk-icon="icon: comment ; ratio:1.2"></span>
    </div>
    <div class="uk-card uk-card-default uk-padding-small uk-width-1-1">
        <form method="post" action="#" enctype="multipart/form-data">

            <textarea name="text_post" class="uk-textarea"></textarea>

            <hr>
            <div uk-grid>
                <div uk-form-custom>
                    <input type="file" name="image_post[]" accept="image/*" multiple>
                    <!-- <button type="button"></button> -->
                    <a href="" class="uk-width-auto" uk-icon="icon: camera ; ratio:2"></a>
                </div>

                <div class="uk-width-1-6">
                </div>

                <input name="btnSubmit" type="submit" class="uk-button-small uk-button-primary uk-width-expand">

            </div>
        </form>
    </div>
</div>
