<div class="uk-card uk-card-default uk-width-expand uk-margin-small-top uk-margin-small-left uk-padding-remove-left">
    <div class="uk-card uk-card-default uk-padding-small uk-width-1-1" style="background-color:#d9d9d9">
        À Propos
        <span uk-icon="icon: info ; ratio:1.2"></span>
    </div>
    <div class="uk-card uk-card-default uk-padding-small uk-width-1-1">

        <div>
            <span uk-icon="icon: facebook ; ratio:1.2"></span>
            <a class="uk-text-meta" href="http://facebook.com/CFPT">http://facebook.com/CFPT</a>
        </div>

        <br>

        <div>
            <span uk-icon="icon: youtube ; ratio:1.2"></span>
            <a class="uk-text-meta" href="http://youtube.com">http://youtube.com/CFPT</a>
        </div>

    </div>
</div>
