<?php

include_once './models/utils.php';
include_once './models/dbBlog.php';

if (isset($_POST['btnSubmit']))
{
    ctrCreatePost();
}

if (isset($_POST['btnEditPost']))
{
    ctrUpdatePost();
}

if (isset($_POST['btnAddImages']))
{
    ctrAddImageToPost();
}

if (isset($_POST['btnDeletePost']))
{
    ctrDeletePost();
}

if (isset($_POST['btnDeleteImage']))
{
    ctrDeleteImage();
}

function ctrCreatePost()
{
    // Input text
    if (isset($_POST['text_post']))
    {
        // Input image
        if (isset($_FILES['image_post']))
        {
            $tabImages = array();
            try
            {
                // Copy from temp to IMG_PATH and resize images
                $tabImages = manageImages($_FILES, 580, 580, IMG_PATH);
                insertTextAndImages($_POST['text_post'], $tabImages);
            }
            catch (Exception $e)
            {
                foreach ($tabImages as $value)
                {
                    @unlink(IMG_PATH.$value);
                }
            }
        }
        else {
            echo "Veuillez selectionner une image.";
        }
    }
}

function ctrUpdatePost()
{
    editPost($_POST['idPost'], $_POST['textPost']);
}

function ctrAddImageToPost()
{
    // Input image
    if (isset($_FILES['image_post']))
    {
        $tabImages = array();
        try
        {
            // Copy from temp to IMG_PATH and resize images
            $tabImages = manageImages($_FILES, 580, 580, IMG_PATH);
            addImagesToPost($_POST['idPost'], $tabImages);
        }
        catch (Exception $e)
        {
            foreach ($tabImages as $value)
            {
                @unlink(IMG_PATH.$value);
            }
        }
    }
    else {
        echo "Veuillez selectionner une image.";
    }
}

function ctrDeletePost()
{
    deletePost($_POST['idPost']);
}

function ctrDeleteImage()
{
    deleteImageDB($_POST['idImage']);
    deleteImageFile($_POST['linkImage']);
}

function showPosts()
{
    $result = "";
    $modalsImages= "";

    $tabPosts = getPosts();

    foreach  ($tabPosts as $row) {
        $date = new DateTime($row['datePost']);
        $result =  $result.'
        <form action="#" method="post" enctype="multipart/form-data">
            <div class="uk-card uk-card-default uk-padding-small uk-width-1-1">
                <div class="uk-card uk-card-default uk-card-body uk-width-1-1 uk-padding-small" style="background-color:#d9d9d9">

                    <h3 class="uk-card-title">
                        '.$date->format("j F Y, g:i a").'

                        <div uk-form-custom class="uk-align-right uk-margin-remove-left">
                            <button uk-toggle="target: #modalDeletePost'.$row['idPost'].'" type="submit" name="btnAlertDeletePost">
                            <a href="" class="uk-width-auto" uk-icon="icon: trash ; ratio:1"></a>
                        </div>

                        <div uk-form-custom class="uk-align-right">
                            <button uk-toggle="target: #modalEditPost" type="submit" name="btnAlertEditPost">
                            <a href="" class="uk-width-auto" uk-icon="icon: pencil ; ratio:1"></a>
                        </div>
                    </h3>

                    <p>'.$row['textPost'].'</p>';

                    $tabImages = getImagesByPostId($row['idPost']);
                    foreach  ($tabImages as $rowImage) {
                        $result =  $result.'<img src="'.$rowImage['linkImage'].'" style="margin-top:10px">
                        <div uk-form-custom class="uk-align-right uk-margin-remove-left">
                            <button uk-toggle="target: .modalDeleteImage'.$rowImage['idImage'].'" type="submit" name="btnAlertDeleteImage">
                            <a href="" class="uk-width-auto" uk-icon="icon: trash ; ratio:1"></a>
                        </div>';

                        $modalsImages= $modalsImages.'<div class="modalDeleteImage'.$rowImage['idImage'].'" uk-modal>
                            <div class="uk-modal-dialog uk-modal-body">
                                <form action="#" method="post" name="'.$rowImage['idImage'].'" id="'.$rowImage['idImage'].'">
                                    <h2 class="uk-modal-title">Attention</h2>
                                    <p>Vous allez supprimer cette image de façon définitive.</p>
                                    <p class="uk-text-right">
                                        <input type="hidden" name="idImage" value="'.$rowImage['idImage'].'">
                                        <input type="hidden" name="linkImage" value="'.$rowImage['linkImage'].'">
                                        <button class="uk-button uk-button-default uk-modal-close" type="button">Annuler</button>
                                        <input class="uk-button uk-button-primary" type="submit" value="Continuer" name="btnDeleteImage">
                                    </p>
                                </form>
                            </div>
                        </div>';
                    }

                    $result =  $result.'

                    <div class="uk-width-1-1 uk-margin-small-bottom uk-margin-medium-top uk-text-center">
                        <div uk-form-custom>
                            <input type="file" name="image_post[]" accept="image/*" multiple>
                            <a href="" class="uk-width-auto" uk-icon="icon: plus-circle ; ratio:5"></a>
                        </div>
                    </div>

                    <div class="uk-width-auto uk-margin-small-bottom uk-margin-small-top uk-text-center">
                        <input type="hidden" name="idPost" value="'.$row['idPost'].'">
                        <input type="submit" class="uk-button uk-button-primary" name="btnAddImages" value="Confirmer l\'ajout des images"/>
                    </div>

                </div>
            </div>
        </form>

        <div id="modalEditPost" uk-modal>
            <div class="uk-modal-dialog uk-modal-body">
                <form action="#" method="post">
                    <h2 class="uk-modal-title">Modification du post</h2>
                    <textarea name="textPost" class="uk-textarea"></textarea>
                    <p class="uk-text-right">
                            <input type="hidden" name="idPost" value="'.$row['idPost'].'">
                            <button class="uk-button uk-button-default uk-modal-close" type="button">Annuler</button>
                            <input class="uk-button uk-button-primary" type="submit" value="Continuer" name="btnEditPost">
                    </p>
                </form>
            </div>
        </div>

        <div id="modalDeletePost'.$row['idPost'].'" uk-modal>
            <div class="uk-modal-dialog uk-modal-body">
                <form action="#" method="post">
                    <h2 class="uk-modal-title">Attention</h2>
                    <p>Vous allez supprimer ce post de façon définitive.</p>
                    <p class="uk-text-right">
                            <input type="hidden" name="idPost" value="'.$row['idPost'].'">
                            <button class="uk-button uk-button-default uk-modal-close" type="button">Annuler</button>
                            <input class="uk-button uk-button-primary" type="submit" value="Continuer" name="btnDeletePost">
                    </p>
                </form>
            </div>
        </div>';
    }

    return $result.$modalsImages;
}

?>
